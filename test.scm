#!/usr/local/bin/csi -s

(import sdl-mixer)

(open-audio #:sampling-rate 44100 
            #:sample-format #x0008
            ;#:channels
            #:chunk-size 1024
            #:volume 64
            )

(define test-samples
 (lambda ()
  (define sample1 (load-sample "rain_2.wav"))
  (define sample2 (load-sample "The_Ghost_in_Your_Piano_-_04_-_Lullaby.mp3"))
  (print "Playing multiple channels together.")
  (play-sample sample1)
  (play-sample sample2)
  (sleep 5)
  (halt-channel -1)
  (channel-finished (lambda (chan) (print "Channel " chan " is finished.\n")))
  (define chan1
   (play-sample sample1 #:repeat 1 #:fadein 1000 #:duration 1000000))
  (sleep 5)
  (pause-channel chan1)
  (define chan2
   (play-sample sample2 #:repeat 1 #:fadein 2000 #:duration 150000))
  (sleep 5)
  (print "Is channel " chan2 " paused? Should print false:")
  (print (channel-paused? chan2))
  (print "Is channel " chan1  " paused? Should print true:")
  (print (channel-paused? chan1))
  (sleep 2)
  (resume-channel -1) ;chan1)
  (halt-channel chan2 #:fadeout 1000)
  (print "Is channel " chan2 " playing? Should print false:")
  (print (channel-playing? chan2))
  (print "Is channel " chan1 "  playing? Should print true:")
  (print (channel-playing? chan1))
  (sleep 2)
  (halt-channel chan1 #:fadeout 1000)
  (define chan3
   (play-sample sample1 #:repeat 1 #:fadein 1000 #:duration 10000000))
  (sleep 2)
  (pause-channel chan3)
  (sleep 2)
  (resume-channel chan3)))

(define test-music
 (lambda ()
  (define music (load-music "Sergey_Cheremisinov_-_05_-_The_Healing.mp3"))
  (music-finished (lambda () (print "Music finished.")))
  (play-music music #:repeat 1 #:fadein 2000 #:volume 50)
  (sleep 5)
  (print "This should print true:")
  (print (music-playing?))
  (sleep 10)
  (print "Pausing music.")
  (pause-music)
  (sleep 10)
  (print "Resuming music.")
  (resume-music)
  (sleep 10)
  (print "Rewinding music.")
  (rewind-music)
  (sleep 10)
  (print "Chaninging volume from " (music-volume) " to 25.")
  (music-volume 25)
  (sleep 10)
  (halt-music #:fadeout 1000)
  (sleep 2)
  (print "This should print false:")
  (print (music-playing?))))

(test-samples)
(test-music)

(close-audio)
